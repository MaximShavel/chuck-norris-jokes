const selectors = {
  buttonLike: ".js-button-like",
  buttonAdd: ".js-button-add-jokes",
  spinner: ".js-spinner-jokes",
  containerJokes: ".js-container-jokes",
  templateJokes: ".js-template-jokes",
  templateJokesDescription: ".js-jokes-card-description",
};

const classes = {
  spinnerVisible: "spinner_visible",
};

const containerJokes = document.querySelector(selectors.containerJokes);
const templateJokes = document.querySelector(selectors.templateJokes);
const buttonAdd = document.querySelector(selectors.buttonAdd);
const buttonLike = templateJokes.querySelector(selectors.buttonLike);
const spinner = document.querySelector(selectors.spinner);
const url = "https://api.chucknorris.io/jokes/random";
let pusk;
let processStarted = false;
let likedTheJoke = false;
let jokesList = [];

buttonAdd.addEventListener("click", function () {
  if (!processStarted) {
    processStarted = true;
    spinner.classList.add(classes.spinnerVisible);
    pusk = setInterval(() => {
      fetch(url)
        .then((response) => {
          if (response.ok) return response.json();
          else return Promise.reject(response.status);
        })
        .then((json) => {
          const templateJokes = document
            .querySelector(selectors.templateJokes)
            .content.cloneNode(true);
          templateJokes
            .querySelector(selectors.buttonLike)
            .addEventListener("click", onLiked);
          templateJokes.querySelector(
            selectors.templateJokesDescription
          ).textContent = json.value;
          containerJokes.append(templateJokes);
        })
        .catch((error) => new Error(error));
    }, 500);
  } else {
    clearInterval(pusk);
    processStarted = false;
    spinner.classList.remove(classes.spinnerVisible);
  }
});

function checkedLength() {
  if (localStorage.length >= 10) {
    localStorage.removeItem(localStorage.key(0));
  } else {
  }
}

const addItemLiked = (item) => {
  if(!jokesList.includes(item)) {
    if(jokesList.length < 10) {
      jokesList.push(item);
    } else {
      jokesList.shift();
      jokesList.push(item);
    }
  } else {
    let pos = jokesList.indexOf(item);
    jokesList.splice(pos, 1);
  }
  localStorage.setItem('jokesList', JSON.stringify(jokesList));
  }


function onLiked(e) {
  const button = e.target.closest(selectors.buttonLike);
  button.firstElementChild.classList.toggle("card_active");
  const text = button.nextElementSibling.textContent;
  addItemLiked(text);
}
