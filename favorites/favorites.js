const selectors = {
  buttonDelete: ".js-button-delete",
  buttonClear: ".js-button-clear-jokes",
  spinnerAddJokes: ".js-spinner-add-jokes",
  containerJokes: ".js-container-jokes",
  templateJokes: ".js-template-jokes",
  templateJokesDescription: ".js-jokes-card-description",
};

const classes = {
  buttonDisable: "button_disable",
};

const containerJokes = document.querySelector(selectors.containerJokes);
const templateJokes = document.querySelector(selectors.templateJokes);
const buttonClear = document.querySelector(selectors.buttonClear);
const buttonDelete = templateJokes.querySelector(selectors.buttonDelete);
const ul = document.querySelector(".favorites__list");
let jokesList = JSON.parse(localStorage.getItem("jokesList"));

if(!jokesList) {
  jokesList = [];
  buttonClear.setAttribute("disabled", true);
}

for (let i = 0; i < jokesList.length; i++) {
  const templateJokes = document.querySelector(selectors.templateJokes).content.cloneNode(true);
  let buttonDelete = templateJokes.querySelector(selectors.buttonDelete);
  buttonDelete.addEventListener("click", onDelete);
  templateJokes.querySelector(selectors.templateJokesDescription).textContent = jokesList[i];
  containerJokes.append(templateJokes);
}


function onDelete(e) {
  const button = e.target.closest(selectors.buttonDelete);
  const key = button.nextElementSibling.textContent;
  let pos = jokesList.indexOf(key);
  jokesList.splice(pos, 1);
  localStorage.setItem('jokesList', JSON.stringify(jokesList));
  button.parentElement.remove();
  if (!jokesList.length) {
    buttonClear.setAttribute("disabled", true);
  }
}

buttonClear.addEventListener("click", function () {
  containerJokes.innerHTML = "";
  jokesList = [];
  localStorage.clear();
  if (!jokesList.length) {
    buttonClear.setAttribute("disabled", true);
  }

});
